variables:
  LIGO_REGISTRY_IMAGE_BASE_NAME: "${CI_PROJECT_PATH}/${CI_PROJECT_NAME}"
  WEBIDE_IMAGE_TAG: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide:${CI_COMMIT_SHORT_SHA}"
  WEBIDE_NEW_IMAGE_TAG: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide_new:${CI_COMMIT_SHORT_SHA}"
  WEBIDE_FRONTEND_NEW_IMAGE_TAG: "registry.gitlab.com/${CI_PROJECT_PATH}/ligo_webide_frontend_new:${CI_COMMIT_SHORT_SHA}"
  LIGO_IMAGE_TAG: "ligo:${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_ID}"

stages:
  - pretooling                      # changelog
  - build                           # build, test, produces binaries/.deb/doc within docker
  - post-build-action               # Post build action
  - docker-extract                  # extract what have been built and expose them as artifact
  - tooling                         # build miscellaneous tooling-related things (website, changelog..)
  - push                            # deploy docker images, pages, releases (tag)
  - deploy                          # deploy web IDE, website preview
  - release                         # Manage branch matching ^release/*

include:
  # jobs for managing process around changelog generation
  - '/.ci/.gitlab-ci-changelog.yml'
  # jobs for managing process around release
  - '/.ci/.gitlab-ci-release.yml'

.submodule-clone:
  before_script:
    - sed -i 's;without-tests;prometheansacrifice/v12.0-ligo-windows-compat;' .git/modules/vendors/tezos-ligo/config
    - git submodule sync --recursive
    - git submodule update --init --recursive --remote

.nix:
  extends: .submodule-clone
  before_script:
    - find "$CI_PROJECT_DIR" -path "$CI_PROJECT_DIR/.git" -prune -o "(" -type d -a -not -perm -u=w ")" -exec chmod --verbose u+w {} ";"
    - export COMMIT_DATE="$(git show --no-patch --format=%ci)"

changelog:
  stage: pretooling
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /^release/.*$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - ./scripts/changelog-generation.sh
  artifacts:
    paths:
      - changelog.md
      - changelog.txt
      - release-notes.md
      - release-notes.txt

to_dos:
  stage: pretooling
  only:
    - merge_requests
    - /^.*-run-dev$/
  script:
    - ./scripts/list-to-dos.sh > to-dos.html
  artifacts:
    paths:
      - to-dos.html
  
# TODO: https://docs.gitlab.com/ee/ci/yaml/#onlychangesexceptchanges ?
docker_build:
  extends: .submodule-clone
  stage: build
  dependencies:
    - changelog
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /^release/.*$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - DOCKER_BUILDKIT=1 docker build .
      --build-arg=ci_commit_tag=$CI_COMMIT_TAG
      --build-arg=ci_commit_sha=$CI_COMMIT_SHA
      --build-arg=ci_commit_timestamp=$CI_COMMIT_TIMESTAMP
      --build-arg=changelog_path=changelog.txt
      -t ${LIGO_IMAGE_TAG}
    - docker save -o ligo.tar.gz ${LIGO_IMAGE_TAG}
  after_script:
    - docker image rm "${LIGO_IMAGE_TAG}" >/dev/null 2>&1 || true
  artifacts:
    expose_as: "Ligo docker image - light -"
    paths:
      - ligo.tar.gz

docker_extract:
  stage: docker-extract
  only:
    - merge_requests
    - dev
    - /^.*-run-dev$/
    - /^release/.*$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  dependencies :
    - docker_build
  script:
    - docker load -i ligo.tar.gz
    - docker cp $(docker create ${LIGO_IMAGE_TAG}):/root/ligo .
    - chmod +rwx ligo
    # Check that the binary is truly static and has 0 dependencies
    # previous command, but it seems statically meant something a bit different.
    # - ldd ligo | grep 'statically' # TODO: there is maybe a better way to do that ?
    # the string depends on ldd...
    - if ! ldd ligo; then echo "ldd failed, i.e. expecting that it is statically linked."; else echo "Expected ldd ligo to fail (not a dynamic executable)."; fi
    - docker cp -La $(docker create ${LIGO_IMAGE_TAG}):/root/doc .
    - docker cp $(docker create ${LIGO_IMAGE_TAG}):/root/highlighting ./highlighting
    # Generate ligo.deb
    - ./scripts/create-package-deb-file.sh
  after_script:
    - docker image rm -f "${LIGO_IMAGE_TAG}" >/dev/null 2>&1 || true
  artifacts:
    paths:
      - ligo
      - ligo.deb
      - doc
      - highlighting

check-generated-highlighting:
  stage: tooling
  dependencies:
    - docker_extract
  script:
    - scripts/diff_dir.sh highlighting/emacs tools/emacs
    - scripts/diff_dir.sh highlighting/vim/ftdetect tools/vim/ligo/start/ligo/ftdetect
    - scripts/diff_dir.sh highlighting/vim/plugin tools/vim/ligo/start/ligo/plugin
    - scripts/diff_dir.sh highlighting/vim/syntax tools/vim/ligo/start/ligo/syntax
    - scripts/diff_dir.sh highlighting/vscode tools/lsp/vscode-plugin/syntaxes
    - scripts/diff_dir.sh highlighting/textmate tools/ligo-syntax-highlighting/textmate
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - tools/ligo-syntax-highlighting/**/*
      when: always

xrefcheck:
  stage: tooling
  only:
    - merge_requests
  needs:
    - job: changelog
      artifacts: true
  script:
    - wget https://github.com/serokell/xrefcheck/releases/download/v0.2.1/xrefcheck-x86_64-linux 
    - chmod 777 ./xrefcheck-x86_64-linux
    - ./xrefcheck-x86_64-linux

.docker-image:
  stage: push
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind

docker-push:
  stage: push
  extends: .docker-image
  dependencies:
    - docker_build
  needs:
    - docker_build
  only:
    - dev
    - /^.*-run-dev$/
    - /[0-9]+\.[0-9]+\.[0-9]+/
  script:
    - echo ${LIGO_REGISTRY_PASSWORD} | docker login -u ${LIGO_REGISTRY_USER} --password-stdin
    - docker load -i=./ligo.tar.gz
    - if test "$CI_COMMIT_REF_NAME" = "dev"; then export LIGO_TAG=next; else if echo "$CI_COMMIT_TAG" | grep -E "[0-9]+\.[0-9]+\.[0-9]+"; then export LIGO_TAG="$CI_COMMIT_TAG"; else export LIGO_TAG=next-attempt; fi; fi
    - export LIGO_REGISTRY_FULL_NAME="${LIGO_REGISTRY_IMAGE_BUILD:-ligolang/ligo}:$LIGO_TAG"
    - docker tag "${LIGO_IMAGE_TAG}" "${LIGO_REGISTRY_FULL_NAME}"
    - docker push "${LIGO_REGISTRY_FULL_NAME}"
  after_script:
    - docker image rm "${LIGO_IMAGE_TAG}" >/dev/null 2>&1 || true

# docker-large-push: ?
#   extends: docker-push
#   dependencies:
#     - docker-large
#   needs:
#     - docker-large
#   script:
#     - echo "${LIGO_REGISTRY_PASSWORD}" | docker login -u "${LIGO_REGISTRY_USER}" --password-stdin
#     - docker load -i=./ligo.tar.gz
#     - if test "$CI_COMMIT_REF_NAME" = "dev"; then export LIGO_TAG=next-large; else if echo "$CI_COMMIT_TAG" | grep -E "[0-9]+\.[0-9]+\.[0-9]+"; then export LIGO_TAG="$CI_COMMIT_TAG-large"; else export LIGO_TAG=next-attempt-large; fi; fi
#     - export LIGO_REGISTRY_FULL_NAME="${LIGO_REGISTRY_IMAGE_BUILD:-ligolang/ligo}:$LIGO_TAG"
#     - docker tag ligo "${LIGO_REGISTRY_FULL_NAME}"
#     - docker push "${LIGO_REGISTRY_FULL_NAME}"

webide-backend-build-and-push-backend:
  extends: .docker-image
  stage: deploy
  rules:
    # Only deploy docker when from the dev branch or a merge request AND on the canonical ligolang/ligo repository
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_PROJECT_PATH == "ligolang/ligo"'
      changes:
        - tools/webide/**/*
        - src/test/examples/**/*
      when: always
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always
  script:
    - echo "${CI_BUILD_TOKEN}" | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - cp -r src/test/examples tools/webide/packages/client/examples
    - docker build tools/webide --build-arg EXAMPLES_DIR_SRC=packages/client/examples -t ${WEBIDE_IMAGE_TAG}
    - docker push "${WEBIDE_IMAGE_TAG}"
  after_script:
    - docker image rm "${WEBIDE_IMAGE_TAG}" >/dev/null 2>&1 || true

webide-new-backend-build-and-push-backend:
  extends: .docker-image
  stage: deploy
  needs:
    - job: changelog
      artifacts: false
  rules:
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      changes:
        - tools/webide-new/ligo-webide-backend/**/*
      when: always
  script:
    - echo "${CI_BUILD_TOKEN}" | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - docker build tools/webide-new/ligo-webide-backend -t ${WEBIDE_NEW_IMAGE_TAG}
    - docker push "${WEBIDE_NEW_IMAGE_TAG}"
  after_script:
    - docker image rm "${WEBIDE_NEW_IMAGE_TAG}" >/dev/null 2>&1 || true

webide-new-frontend-build-and-push:
  extends: .docker-image
  stage: deploy
  needs:
    - job: changelog
      artifacts: false
  rules:
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      changes:
        - tools/webide-new/ligo-webide-frontend/ligo-ide/**/*
      when: always
  script:
    - echo "${CI_BUILD_TOKEN}" | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - docker build tools/webide-new/ligo-webide-frontend/ligo-ide -t ${WEBIDE_FRONTEND_NEW_IMAGE_TAG}
    - docker push "${WEBIDE_FRONTEND_NEW_IMAGE_TAG}"
  after_script:
    - docker image rm "${WEBIDE_FRONTEND_NEW_IMAGE_TAG}" >/dev/null 2>&1 || true

.website:
  stage: push
  tags:
    - docker
  image: node:16.17.0-buster
  needs:
    - job: changelog
      artifacts: true
  script:
    - export DOC_PATH=./gitlab-pages/docs
    - export WEBSITE_PATH=./gitlab-pages/website
    - cp -f ./changelog.md $DOC_PATH/intro/changelog.md
    - export LATEST_VERSION=$(ls -1r $WEBSITE_PATH/versioned_docs | sed -n '1p')
    - echo $LATEST_VERSION
    - cp -f ./changelog.md $WEBSITE_PATH/versioned_docs/$LATEST_VERSION/intro/changelog.md
    - npm --prefix $WEBSITE_PATH ci
    - npm --prefix $WEBSITE_PATH run build
    - cp -Lr --no-preserve=mode,ownership,timestamps $WEBSITE_PATH/build public
    - touch to-dos.html; cp to-dos.html public/to-dos.html
  artifacts:
    paths:
      - public

pages:
  extends: .website
  rules:
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      when: always

pages-attempt:
  extends: .website
  needs:
    - job: xrefcheck
    - job: changelog
      artifacts: true
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - gitlab-pages/**/*
      when: on_success

deploy_website_preview:
  stage: deploy
  image: node:16.17.0-buster
  tags:
    - docker
  needs:
    - job: pages-attempt
      artifacts: true
  script: 
    - apt-get update
    - apt-get install -y --no-install-recommends jq
    - npm install netlify-cli
    - npx netlify deploy --site $NETLIFY_SITE_ID_LIGOLANG --dir=./public --auth $NETLIFY_AUTH_TOKEN --json --alias="$CI_MERGE_REQUEST_IID-$CI_COMMIT_SHORT_SHA" > netlify.json  
    - export NETLIFY_DEPLOY_URL=$(jq -r .deploy_url netlify.json)
    - export NETLIFY_DEPLOY_LOGS=$(jq -r .logs netlify.json)
    - echo "The URL for this deployment is $NETLIFY_DEPLOY_URL"
    - echo "Logs for this deployment can be found at $NETLIFY_DEPLOY_LOGS"
    - '[ ! -z "$CI_MERGE_REQUEST_IID" ] && curl POST https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes -d "{\"body\": \"A new deploy preview is available on Netlify at $NETLIFY_DEPLOY_URL, you can view the logs at $NETLIFY_DEPLOY_LOGS\"}" --header "Content-Type: application/json" --header "Private-Token: $GITLAB_PERSONAL_API_PRIVATE_TOKEN"'  
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - gitlab-pages/**/*
      when: on_success

# This job will deploy the web-ide docker container to the test server.
deploy-handoff-feature:
  # Handoff deployment duties to private repo.
  stage: deploy
  variables:
    APP_NAME: "web-ide-backend"
    NEW_VERSION: "${CI_COMMIT_SHORT_SHA}"
    ENV: "non-prod"
  trigger: 
    project: ligolang/kubernetes-infrastructure
  rules:
    # Only deploy handoff when from an MR AND on the canonical ligolang/ligo repository.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_PROJECT_PATH == "ligolang/ligo"'
      changes:
        - tools/webide/**/*
        - src/test/examples/**/*
      when: always

deploy-handoff:
  # Handoff deployment duties to private repo
  stage: deploy
  variables:
    APP_NAME: "web-ide-backend"
    NEW_VERSION: "${CI_COMMIT_SHORT_SHA}"
    ENV: "prod"
  trigger: 
    project: ligolang/kubernetes-infrastructure
  rules:
    # Only deploy handoff when from the dev branch AND on the canonical ligolang/ligo repository
    - if: '$CI_COMMIT_REF_NAME == "dev" && $CI_PROJECT_PATH == "ligolang/ligo"'
      changes:
        - tools/webide/**/*
        - src/test/examples/**/*
      when: always

# This job builds the ligo-webide front-end from the tools/webide/ directory and deploys it as a Branch Deploy to Netlify using the netlify-cli tool.
# After the deployment is done a comment is posted to the MR with the draft site URL as well as the Netlify logs
build-preview-ligo-webide-front-end:
  stage: deploy
  image: node:12.20.0-buster
  tags:
    - docker
  script:
    - apt-get update
    - apt-get install -y --no-install-recommends jq
    - cd ./tools/webide/
    - sed -i "s/temphost/$CI_MERGE_REQUEST_IID-$CI_COMMIT_SHORT_SHA/g" ./packages/client/feature-netlify.toml
    - cat ./packages/client/feature-netlify.toml
    - mv ./packages/client/feature-netlify.toml ./packages/client/netlify.toml
    - yarn install
    - CI= yarn workspaces run build
    - cd ./packages/client
    - npm install netlify-cli@11.1.0-rc.2
    - npx netlify deploy --site $NETLIFY_SITE_ID --auth $NETLIFY_AUTH_TOKEN --json --alias="$CI_MERGE_REQUEST_IID-$CI_COMMIT_SHORT_SHA" > netlify.json
    - export NETLIFY_DEPLOY_URL=$(jq -r .deploy_url netlify.json)
    - export NETLIFY_DEPLOY_LOGS=$(jq -r .logs netlify.json)
    - echo "The URL for this deployment is $NETLIFY_DEPLOY_URL"
    - echo "Logs for this deployment can be found at $NETLIFY_DEPLOY_LOGS"
    - '[ ! -z "$CI_MERGE_REQUEST_IID" ] && curl POST https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes -d "{\"body\": \"A new deploy preview is available on Netlify at $NETLIFY_DEPLOY_URL, you can view the logs at $NETLIFY_DEPLOY_LOGS\"}" --header "Content-Type: application/json" --header "Private-Token: $GITLAB_PERSONAL_API_PRIVATE_TOKEN"'
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - tools/webide/**/*
        - src/test/examples/**/*
      when: always

# This job builds the ligo-webide front-end from the tools/webide/ directory and does a production deployment to Netlify and publishes the site using the netlify-cli tool.
build-ligo-webide-front-end:
  stage: deploy
  image: node:12.20.0-buster
  tags:
    - docker
  script:
    - apt-get update
    - cd ./tools/webide/
    - mv ./packages/client/prod-netlify.toml ./packages/client/netlify.toml
    - yarn install
    - CI= yarn workspaces run build
    - cd ./packages/client
    # new version of CLI is incompatible with node:12:20. 
    - npm install netlify-cli@11.1.0-rc.2
    - npx netlify deploy --site $NETLIFY_SITE_ID --auth $NETLIFY_AUTH_TOKEN --json --prod > netlify.json
  rules:
    - if: '$CI_COMMIT_BRANCH == "dev"'
      changes:
        - tools/webide/**/*
        - src/test/examples/**/*
      when: always
