let one (type a) (_ : a) = 1n

let main (_, _ : int list * nat) : (operation list * nat) =
    [], (one [])